<?php
// starting load all dependencies
$loader = require 'vendor/autoload.php';

$classMap = [
    "Component\\"             => [__DIR__ . '/src'],
    "Component\\PaymentApi\\" => [__DIR__ . '/src/Component'],
    "Controller\\"            => [__DIR__ . '/src'],
    "Entity\\"                => [__DIR__ . '/src'],
    "Entity\\Repository\\"    => [__DIR__ . '/src/Entity'],
];

foreach ($classMap as $namespace => $path) {
    $loader->set($namespace, $path);
}

// finish
try {
    \ob_start();
    $container = \Component\ServiceContainer::create();

    /** @var \Component\Router $router */
    $router = $container->get('router');
    echo $router->getResponse();
    \ob_end_flush();
    exit(200);
} catch (\Throwable $e) {
    if (0 === $e->getCode()) {
        $code = 400;
    }
    http_response_code($code);
    header(($_SERVER['SERVER_PROTOCOL'] ?? 'HTTP/1.0') . " {$code} {$e->getMessage()}");
    echo $code . ' ' . $e->getMessage() . ' Stack trace: ' . $e->getTraceAsString();
    \ob_end_flush();
    exit($code);
}





