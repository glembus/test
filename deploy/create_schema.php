<?php
$loader = require __DIR__ . '/../vendor/autoload.php';

//Loading connection parameters
$connParams = loadParameters();

//storing database nameand removing it from parameters
$dbName               = $connParams['dbname'];
$connParams['dbname'] = null;

//create connection
$conn = \Doctrine\DBAL\DriverManager::getConnection($connParams);
//trying to create database;
try {
    writeLn('Checking access to database');
    $databases = $conn->getSchemaManager()->listDatabases();
    if (!\in_array($dbName, $databases)) {
        $conn->getSchemaManager()->createDatabase($dbName);
        writeLn('Database created');
    } else {
        writeLn('Database already exists.');
    }
} catch (\Throwable $e) {
    writeLn('Unable to create database. Try create it manually and try again. Reason: ' . $e->getMessage());
    exit(1);
}
//closing connection
$conn->close();

//add database name to parameters and open new connection
$connParams['dbname'] = $dbName;
$conn                 = \Doctrine\DBAL\DriverManager::getConnection($connParams);

writeLn('Database ' . $connParams['dbname'] . ' created');
$tableList    = $conn->getSchemaManager()->listTableNames();
$schemaConfig = new \Doctrine\DBAL\Schema\SchemaConfig();
$schemaConfig->setName($connParams['dbname']);
$schema = new \Doctrine\DBAL\Schema\Schema([], [], $schemaConfig);
try {
    getCreateTableSql($schema, $tableList);
    $sqls = $schema->toSql($conn->getDatabasePlatform());
    foreach ($sqls as $sql) {
        $conn->exec($sql);
    }
} catch (Exception $e) {
    writeLn($e->getMessage());
    writeLn('continue...');
}

writeLn('Schema was updated');

exit(0);

/**
 * Add a new tables using DBAL schema.
 * Just add a new table in switch statement and add create code inside it.
 *
 * @param \Doctrine\DBAL\Schema\Schema $schema
 * @param                              $tables
 *
 * @return void
 */
function getCreateTableSql(\Doctrine\DBAL\Schema\Schema &$schema, $tables)
{
    if (!\in_array('user', $tables)) {
        $userTable = $schema->createTable('user');
        $userTable->addColumn('id', \Doctrine\DBAL\Types\Type::INTEGER)->setAutoincrement(true);
        $userTable->addColumn('ssid', Doctrine\DBAL\Types\Type::STRING)->setLength(255);
        $userTable->addColumn('first', \Doctrine\DBAL\Types\Type::STRING)->setLength(50);
        $userTable->addColumn('last', \Doctrine\DBAL\Types\Type::STRING)->setLength(50);
        $userTable->addColumn('phone', \Doctrine\DBAL\Types\Type::STRING)->setLength(12);
        $userTable->addColumn('street', \Doctrine\DBAL\Types\Type::STRING)->setLength(100)->setNotnull(false);
        $userTable->addColumn('home', \Doctrine\DBAL\Types\Type::STRING)->setLength(10)->setNotnull(false);
        $userTable->addColumn('city', \Doctrine\DBAL\Types\Type::STRING)->setLength(100)->setNotnull(false);
        $userTable->addColumn('zipCode', \Doctrine\DBAL\Types\Type::STRING)->setLength(10)->setNotnull(false);
        $userTable->addColumn('iban', \Doctrine\DBAL\Types\Type::STRING)->setLength(34)->setNotnull(false);
        $userTable->addColumn('accountOwner',
            \Doctrine\DBAL\Types\Type::STRING)->setLength(100)->setNotnull(false);
        $userTable->addColumn('paymentDataId',
            \Doctrine\DBAL\Types\Type::STRING)->setLength(255)->setNotnull(false);
        $userTable->setPrimaryKey(['id']);
        $userTable->addUniqueIndex(['first', 'last', 'phone']);
        $userTable->addUniqueIndex(['ssid']);
        writeLn('Table `user` scheduled for adding');
    } else {
        writeLn('Table `user` exists');
    }
}

function loadParameters(): array
{
    $config = \Symfony\Component\Yaml\Yaml::parseFile(__DIR__ . '/../config/config.yaml');
    if (!\is_array($config) || !\array_key_exists('dbal', $config)) {
        throw new \Exception('DBAL not configured properly');
    }

    return $config['dbal'];
}

/**
 * Write message to output for logging
 *
 * @param $message
 */
function writeLn($message)
{
    echo $message . PHP_EOL;
}
