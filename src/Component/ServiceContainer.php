<?php

namespace Component;

use Component\PaymentApi\PaymentApi;
use Component\PaymentApi\PaymentService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Entity\Repository\RepositoryInterface;
use Symfony\Component\Yaml\Yaml;

class ServiceContainer
{
    /** @var Connection */
    private $dbConn;

    private $router;

    private $twig;

    /** @var self */
    private static $instance;

    /** @var array */
    private $parameters;

    /** @var Session */
    private $session;

    /** @var PaymentApi */
    private $paymentApi;

    /** @var PaymentService */
    private $paymentService;

    /** @var RepositoryFactory */
    private $repositoryFactory;

    private function __construct()
    {
    }

    /**
     * @return ServiceContainer
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function create(): ServiceContainer
    {
        if (null !== self::$instance) {
            return self::$instance;
        }

        self::$instance    = $instance = new self;
        $instance->session = new Session();
        $instance->loadParameters();
        $instance->dbConn            = DriverManager::getConnection($instance->parameters['dbal']);
        $instance->twig              = new \Twig_Environment(new \Twig_Loader_Filesystem(__DIR__ . '/../View'), ['debug' => false]);
        $instance->router            = new Router($instance, new Request());
        $instance->paymentApi        = new PaymentApi($instance->parameters['payment_api']);
        $instance->paymentService    = new PaymentService($instance->paymentApi);
        $instance->repositoryFactory = new RepositoryFactory($instance->dbConn);

        return $instance;
    }

    /**
     * @param $name
     */
    public function get($name)
    {
        return $this->{$name};
    }

    /**
     * @param string $className
     *
     * @return RepositoryInterface
     * @throws \Exception
     */
    public function getRepository(string $className): RepositoryInterface
    {
        return $this->repositoryFactory->getRepository($className);
    }

    private function loadParameters(): void
    {
        $config = Yaml::parseFile(__DIR__ . '/../../config/config.yaml');
        if (!\is_array($config)) {
            throw new \Exception('Invalid configuration file structure');
        }

        if (!\array_key_exists('dbal', $config) || empty($config['dbal'])) {
            throw new \Exception('DBAL not configured properly');
        }

        if (!\array_key_exists('payment_api', $config)) {
            throw new \Exception('Invalid API configuration');
        }

        $this->parameters = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter(string $name): ?array
    {
        if (isset($this->parameters[$name])) {
            return $this->parameters[$name];
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function getServiceContainer(): ?ServiceContainer
    {
        return self::$instance;
    }

    /**
     * {@inheritdoc}
     */
    public function getDbConn(): Connection
    {
        return $this->dbConn;
    }
}
