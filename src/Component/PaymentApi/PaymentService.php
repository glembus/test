<?php

namespace Component\PaymentApi;

use Entity\User;

class PaymentService
{
    private const PAYMENT_URL = '/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    /** @var PaymentApi */
    private $api;

    public function __construct(PaymentApi $paymentApi)
    {
        $this->api = $paymentApi;
    }

    public function sendPaymentData(User $user)
    {
        $this->api->setBody([
            'customerId' => $user->getId(),
            'iban'       => $user->getIban(),
            'owner'      => $user->getAccountOwner(),
        ]);

        $response = $this->api->makeRequest(self::PAYMENT_URL);

        return $response['paymentDataId'] ?? null;
    }

}