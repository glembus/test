<?php

namespace Component\PaymentApi;

class PaymentApi
{
    /** @var string */
    private $baseUri;

    /** @var string */
    private $body;

    public function __construct(string $baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @param array $body
     *
     * @return $this
     * @throws \Exception
     */
    public function setBody(array $body)
    {
        $this->body = \json_encode($body);
        if (\json_last_error()) {
            throw new \Exception('Unable convert body to to JSON. Reason: ' . \json_last_error_msg());
        }

        return $this;
    }

    /**
     * @param string $url
     *
     * @return mixed
     * @throws \Exception
     */
    public function makeRequest(string $url)
    {
        $ch = \curl_init($this->baseUri . $url);
        \curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . \strlen($this->body)
        ]);
        \curl_setopt($ch, CURLOPT_POST, true);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        \curl_setopt($ch, CURLOPT_POSTFIELDS, $this->body);

        $response = curl_exec($ch);
        if (!$response) {
            throw new \Exception(\curl_errno($ch) . ': ' . \curl_error($ch));
        }

        $response = json_decode($response, true);

        if (JSON_ERROR_NONE === \json_last_error()) {
            return $response;
        }

        throw new \Exception('Unable to parse response. Reason: ' . \json_last_error_msg());
    }
}