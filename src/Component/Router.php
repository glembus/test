<?php

namespace Component;

use Symfony\Component\Yaml\Yaml;

class Router
{
    public const HTTP_GET = 'GET';

    public const HTTP_POST = 'POST';

    /**
     * @var Route[]
     */
    private $reachableRoutes;

    private $request;

    private $container;

    /** @var Route */
    private $requestedRoute;

    public function __construct(ServiceContainer $container, Request $request)
    {
        $this->request = $request;
        $this->container = $container;

        $this->loadRoutes();
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        $this->resolveRequest();
        $class    = $this->getClass();
        $action   = $this->requestedRoute->getAction();

        return $class->$action();
    }

    private function loadRoutes(): void
    {
        $reachableRoutes = Yaml::parseFile(__DIR__ . '/../../config/routes.yaml');
        foreach ($reachableRoutes as $route => $description) {
            $this->reachableRoutes[$route] = new Route(
                $description['class'],
                $description['action'],
                $description['method'] ?? 'GET'
            );
        }
    }

    /**
     * @return mixed
     */
    private function getClass()
    {
        $className = $this->requestedRoute->getClass();
        \spl_autoload($className);

        return new $className($this->container, $this->request);
    }

    private function resolveRequest(): void
    {
        if (!\array_key_exists($this->request->getUri(), $this->reachableRoutes)) {
            throw new \Exception('Not Found', 404);
        }

        $this->requestedRoute = $this->reachableRoutes[$this->request->getUri()];
        if (!$this->requestedRoute->isMethodAllowed($this->request->requestMethod)) {
            throw new \Exception('Method not allowed', 405);
        }
    }
}
