<?php

namespace Component;

class Request
{
    private $requestBag;

    public function __construct()
    {
        foreach ($_SERVER as $key => $value) {
            $this->{$this->toCamelCase($key)} = $value;
        }
        $this->requestBag = $this->fillRequestBag();
        unset($this->requestBag['url']);
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        $uri = \parse_url($this->requestUri);

        return $uri['path'];
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasParameter(string $key): bool
    {
        return isset($this->requestBag[$key]);
    }

    /**
     * @param string $key
     * @param string $type
     * @param null   $default
     *
     * @return bool|null
     */
    public function getParameter(string $key, string $type = 'string', $default = null)
    {
        if ($this->hasParameter($key)) {
            $parameter = $this->requestBag[$key];
            return \settype($parameter, $type);
        }

        return $default;
    }

    public function getRequestBag()
    {
        return $this->requestBag;
    }

    /**
     * @return array
     */
    public function getSession(): array
    {
        return $this->fillRequestBody($_SESSION, INPUT_SESSION);
    }

    /**
     * @return array
     */
    private function fillRequestBag(): array
    {
        return \array_merge($this->fillRequestBody($_GET, INPUT_GET), $this->fillRequestBody($_POST, INPUT_POST));
    }

    /**
     * @param $bag
     * @param $filterType
     *
     * @return array
     */
    private function fillRequestBody($bag, $filterType): array
    {
        $result = [];
        foreach ($bag as $key => $value) {
            $result[$key] = \filter_input($filterType, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        return $result;
    }

    /**
     * @param string $string
     *
     * @return string
     */
    private function toCamelCase(string $string): string
    {
        $result = \strtolower($string);

        \preg_match_all('/_[a-z]/', $result, $matches);
        $matches = \reset($matches);
        foreach ($matches as $match) {
            $c      = \str_replace('_', '', strtoupper($match));
            $result = \str_replace($match, $c, $result);
        }

        return $result;
    }
}