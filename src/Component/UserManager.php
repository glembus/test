<?php

namespace Component;

use Component\PaymentApi\PaymentService;
use Entity\Repository\RepositoryInterface;
use Entity\Repository\UserRepository;
use Entity\User;

class UserManager
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var User
     */
    private $user;

    /** @var PaymentService */
    private $paymentServ;

    /**
     * UserManager constructor.
     *
     * @param RepositoryInterface $userRepo
     * @param Session             $session
     * @param PaymentService      $paymentServ
     */
    public function __construct(RepositoryInterface $userRepo, Session $session, PaymentService $paymentServ)
    {
        $this->userRepo    = $userRepo;
        $this->session     = $session;
        $this->paymentServ = $paymentServ;

    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function loadUser(): int
    {
        $this->user = $this->userRepo->findBy(['ssid' => $this->session->getSsid()]);

        if (!$this->user instanceof User) {
            $this->user = new User();

            return User::STEP_LOGIN;
        }

        return $this->user->getFillingLevel();
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        if (null === $this->user) {
            $this->loadUser();
        }

        return $this->user;
    }

    /**
     * @param array $userData
     *
     * @return int
     */
    public function updateUser(array $userData): int
    {
        $refObj = new \ReflectionObject($this->user);
        foreach ($userData as $property => $value) {
            if ($refObj->hasProperty($property)) {
                $property = $refObj->getProperty($property);
                $property->setAccessible(true);
                $property->setValue($this->user, $value);
                $property->setAccessible(false);
            }
        }

        if ($this->user->getIban() && $this->user->getAccountOwner()) {
            $this->user->setPaymentDataId($this->paymentServ->sendPaymentData($this->user));
        }
        return $this->user->getFillingLevel();
    }

    public function flushUser()
    {
        $this->userRepo->updateUser($this->user);
    }
}