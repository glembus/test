<?php

namespace Component;

use Doctrine\DBAL\Connection;
use Entity\Repository\RepositoryInterface;

class RepositoryFactory
{
    /** @var Connection */
    private $conn;

    /**
     * RepositoryFactory constructor.
     *
     * @param Connection $conn
     */
    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param string $class
     *
     * @return RepositoryInterface
     * @throws \Exception
     */
    public function getRepository(string $class): RepositoryInterface
    {
        $ref = new \ReflectionClass($class);
        $nameSpace = $ref->getNamespaceName();
        $className = $ref->getShortName();

        $repositoryName = "\\" . $nameSpace . "\\Repository\\" . $className . 'Repository';
        return new $repositoryName($this->conn);
    }
}