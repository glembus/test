<?php

namespace Component;

class Route
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var string
     */
    private $action;

    /** @var array */
    private $allowedMethods;

    /**
     * Route constructor.
     *
     * @param string $class  Class name
     * @param string $action Action which should be called
     * @param array  $allowedMethods  methods for request
     */
    public function __construct(string $class, string $action, array $allowedMethods)
    {
        $this->class          = $class;
        $this->action         = $action;
        $this->allowedMethods = $allowedMethods;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $requestedMethod
     *
     * @return bool
     */
    public function isMethodAllowed(string $requestedMethod): bool
    {
        return \in_array($requestedMethod, $this->allowedMethods, true);
    }
}
