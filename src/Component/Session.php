<?php

namespace Component;

class Session
{
    /** @var string */
    private $ssid;

    public function __construct()
    {
        $this->start();
        \session_gc();
        $this->get();
    }

    /**
     * @return array
     */
    public function get(): array
    {
        $this->ssid = \session_id();
        if (isset($_SESSION[$this->ssid])) {
            return $_SESSION[$this->ssid];
        }

        return $_SESSION[$this->ssid] = [
            'user' => [
                'id'           => '',
                'first'        => '',
                'last'         => '',
                'phone'        => '',
            ],
        ];
    }

    public function start(): bool
    {
        return \session_start();
    }

    /**
     * @return string
     */
    public function getSsid(): string
    {
        return $this->ssid;
    }

    public function close(): void
    {
        \session_write_close();
    }

    /**
     * @param array $value
     */
    public function set(array $value)
    {
        $_SESSION[\session_id()] = $value;
    }

    public function generateId()
    {
        return \session_create_id();
    }
}
