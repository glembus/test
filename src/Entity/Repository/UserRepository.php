<?php

namespace Entity\Repository;

use Entity\User;
use \Doctrine\DBAL\FetchMode;

class UserRepository extends AbstractRepository
{
    /**
     * @param array $options
     *
     * @return User|null
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findBy(array $options): ?User
    {
        $query = $this->conn->createQueryBuilder()
            ->select('u.*')
            ->from('user', 'u');

        foreach ($options as $field => $value) {
            $query = $query
                ->andWhere("u.{$field} = :{$field}")
                ->setParameter($field, $value);
        }

        $stmt = $this->conn->executeQuery($query->getSQL(), $query->getParameters(), $query->getParameterTypes());
        $stmt->setFetchMode(FetchMode::CUSTOM_OBJECT, User::class);
        $user = $stmt->fetch();

        return $user ?: null;
    }

    /**
     * @param User $user
     *
     * @throws \Exception
     */
    public function updateUser(User $user): void
    {
        if (User::STEP_LOGIN === $user->getFillingLevel()) {
            throw new \Exception('User data filled not enough');
        }
        $query = $this->conn->createQueryBuilder();
        if (null === $user->getId()) {
            $existedUser = $this->findBy(['first' => $user->getFirst(),
                                          'last'  => $user->getLast(),
                                          'phone' => $user->getPhone(),
            ]);
            if ($existedUser instanceof User) {
                $query
                    ->update('user', 'u')
                    ->set('u.ssid', ':ssid')
                    ->where('u.id = :id')
                    ->setParameter('id', $existedUser->getId())
                    ->setParameter('ssid', $user->getSsid())
                    ->execute();

                return;
            }

            $query
                ->insert('user')
                ->setValue('first', ':first')
                ->setValue('last', ':last')
                ->setValue('phone', ':phone')
                ->setValue('ssid', ':ssid')
                ->setParameter('first', $user->getFirst(), 'string')
                ->setParameter('last', $user->getLast(), 'string')
                ->setParameter('phone', $user->getPhone(), 'string')
                ->setParameter('ssid', $user->getSsid(), 'string')
                ->execute();

            return;
        }

        $query
            ->update('user')
            ->set('iban', ':iban')
            ->set('accountOwner', ':accOwner')
            ->set('street', ':str')
            ->set('home', ':home')
            ->set('city', ':city')
            ->set('zipCode', ':zip')
            ->set('paymentDataId', ':payDataId')
            ->andWhere('id = :id')->setParameter('id', $user->getId())
            ->setParameter('iban', (string)$user->getIban(), 'string')
            ->setParameter('accOwner', (string)$user->getAccountOwner(), 'string')
            ->setParameter('str', $user->getStreet(), 'string')
            ->setParameter('home', $user->getHome(), 'string')
            ->setParameter('city', $user->getCity(), 'string')
            ->setParameter('zip', $user->getZipCode(), 'string')
            ->setParameter('payDataId', $user->getPaymentDataId())
            ->execute();
    }
}