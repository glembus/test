<?php

namespace Entity\Repository;

interface RepositoryInterface
{
    public function findBy(array $options);
}