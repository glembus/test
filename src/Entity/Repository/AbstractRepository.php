<?php

namespace Entity\Repository;

use Doctrine\DBAL\Connection;

abstract class AbstractRepository implements RepositoryInterface
{
    /**
     * @var Connection
     */
    protected $conn;

    /**
     * AbstractRepository constructor.
     *
     * @param Connection $conn
     */
    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param array $options
     *
     * @return mixed
     */
    abstract public function findBy(array $options);
}