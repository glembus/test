<?php

namespace Controller;

use Component\Request;
use Component\ServiceContainer;

class Controller
{
    /**
     * @var ServiceContainer
     */
    protected $container;

    /**
     * @var Request
     */
    protected $request;

    /**
     * Controller constructor.
     *
     * @param ServiceContainer $container
     * @param Request          $request
     */
    public function __construct(ServiceContainer $container, Request $request)
    {
        $this->container = $container;
        $this->request   = $request;
    }
}