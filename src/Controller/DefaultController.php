<?php

namespace Controller;

use Component\UserManager;
use Entity\User;

class DefaultController extends Controller
{
    public function main()
    {
        $userManager = new UserManager(
            $this->container->getRepository(User::class),
            $this->container->get('session'),
            $this->container->get('paymentService')
        );
        $userManager->loadUser();

        return $this->container->get('twig')->render('main.html.twig', [
            'user'  => $userManager->getUser(),
            'route' => '/submit',
        ]);
    }

    public function submitUser()
    {
        $userManager = new UserManager(
            $this->container->getRepository(User::class),
            $this->container->get('session'),
            $this->container->get('paymentService')
        );
        $userManager->loadUser();
        $userData = \array_merge(['ssid' => $this->container->get('session')->getSsid()],
            $this->request->getRequestBag());
        $userManager->updateUser($userData);
        $userManager->flushUser();
        if ($payDataId = $userManager->getUser()->getPaymentDataId()) {
            $responseContent = 'You have finished registration. The payment data ID #:' . $payDataId;
        } else {
            $responseContent = $this->container->get('twig')->render('main.html.twig', ['user' => $userManager->getUser()]);
        }

        return $responseContent;
    }
}