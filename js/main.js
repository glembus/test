$(document).on('submit', '.user-data-form', function (e) {
    e.preventDefault();
    if (!isFormValid(this)) {
        return;
    }
    let data = $(this).serialize();
    $.ajax({
        type: 'POST',
        url: '/submit',
        data: data,
        success: function (e) {
            $('.reg-form').html(e);
            $('.message-holder').html('');
        },
        error: function (e) {
            $('.message-holder').html(e.responseText);
        }
    })
});

function isFormValid(form) {
    let valid = true;
    $(form).find('input').each(function(e) {
        let max = $(this).attr('maxlength');
        if ($(this).val().length > max || 0 === $(this).val().length) {
            valid = false;
            $(this).addClass('warning').removeClass('valid');
        } else {
            $(this).addClass('valid').removeClass('warning');
        }
    });

    return valid;
}